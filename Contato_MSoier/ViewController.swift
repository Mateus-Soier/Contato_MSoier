//
//  ViewController.swift
//  Contato_MSoier
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String

}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContato:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContato.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContato[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContato.append(Contato(nome: "Contato 1", numero: "31 98989-0000", email: "contato1@email.com.br", endereco: "Rua do Cristal, 11"))
        listaDeContato.append(Contato(nome: "Contato 2", numero: "31 98989-1111", email: "contato2@email.com.br", endereco: "Rua do Cristal, 22"))
        listaDeContato.append(Contato(nome: "Contato 3", numero: "31 98989-2222", email: "contato3@email.com.br", endereco: "Rua do Cristal, 33"))
        listaDeContato.append(Contato(nome: "Mateus Soier", numero: "31 99999-9999", email: "22001140@aluno.cotemig.com.br", endereco: "Cotemig Floresta"))

        
    }
}

